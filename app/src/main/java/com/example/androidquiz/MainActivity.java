package com.example.androidquiz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView question;
    private ProgressBar progress_bar;
    private TextView breadcrumbs;
    private Button btn_true;
    private Button btn_false;
    private QuizViewModel quizViewModel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (quizViewModel == null)
            quizViewModel = new ViewModelProvider(this).get(QuizViewModel.class);

        question = findViewById(R.id.question);
        progress_bar = findViewById(R.id.progress_bar);
        breadcrumbs = findViewById(R.id.breadcrumbs);
        btn_true = findViewById(R.id.btn_true);
        btn_false = findViewById(R.id.btn_false);
        progress_bar = findViewById(R.id.progress_bar);
        progress_bar.setProgress(10);
        progress_bar.setMax(100);

        /*String data = String.valueOf(quizViewModel.currentIndex);
        Toast.makeText(MainActivity.this, "quizViewModel.currentIndex = " + data, Toast.LENGTH_LONG).show();*/

        if (quizViewModel.currentIndex >= quizViewModel.questionBank.length) {
            Toast.makeText(MainActivity.this, "End game !", Toast.LENGTH_SHORT).show();
        } else {
            question.setText(quizViewModel.getQuestion());
            //question.setText(questionModel[0].getQuestion());
        }

        btn_false.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            check_response(quizViewModel.questionBank[quizViewModel.currentIndex].isRes(), false);
            quizViewModel.increaseIndex();
            progress_bar.setProgress((quizViewModel.currentIndex + 1) * 10);
            change_question();
            breadcrumbs.setText(quizViewModel.getBreadCrumbs());
            }
        });

        btn_true.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            check_response(quizViewModel.questionBank[quizViewModel.currentIndex].isRes(), true);
            quizViewModel.increaseIndex();
            progress_bar.setProgress((quizViewModel.currentIndex + 1) * 10);
            change_question();
            breadcrumbs.setText(quizViewModel.getBreadCrumbs());
            }
        });
    }
    public void check_response(boolean correct, boolean ans) {
        if (correct == ans) {
            Toast.makeText(MainActivity.this, "Correct!", Toast.LENGTH_SHORT).show();
            quizViewModel.increasePuntuation();
        } else {
            Toast.makeText(MainActivity.this, "Incorrect!", Toast.LENGTH_SHORT).show();
        }
    }

    public void change_question() {
        if (quizViewModel.currentIndex <= quizViewModel.questionBank.length-1) {
            question.setText(quizViewModel.getQuestion());
        } else {
            AlertDialog dialog = createSimpleDialog();
            dialog.show();
        }
    }

    public void restartQuiz (){
        quizViewModel.restartQuiz();
        breadcrumbs.setText(quizViewModel.getBreadCrumbs());
        progress_bar.setProgress(0);
        question.setText(quizViewModel.getQuestion());
    }

    /**
     * Create a simple alert dialog
     * @return New dialogue
     */
    public AlertDialog createSimpleDialog() {
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);

        alert.setTitle(R.string.dialog_title)
                .setMessage(getString(R.string.dialog_message)+ " Result: " + quizViewModel.punctuation + "/" + quizViewModel.questionBank.length)
                .setPositiveButton(R.string.pos_btn,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                restartQuiz();
                                //Toast.makeText(MainActivity.this, "CurrentIndex= "+ quizViewModel.currentIndex, Toast.LENGTH_LONG);
                            }
                        })
                .setNegativeButton(R.string.neg_btn,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                finish();
                            }
                        });
        return alert.create();
    }

}


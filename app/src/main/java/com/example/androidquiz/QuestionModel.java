package com.example.androidquiz;

public class QuestionModel {

    private boolean res;
    private int question;


    public QuestionModel(int question, boolean res) {
        this.res = res;
        this.question = question;
    }

    public int getQuestion() {
        return this.question;
    }

    public boolean isRes() {
        return this.res;
    }

    public void setRes(boolean res) {
        this.res = res;
    }

    public void setQuestion(int question) {
        this.question = question;
    }
}

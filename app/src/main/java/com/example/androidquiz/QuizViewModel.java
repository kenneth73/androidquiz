package com.example.androidquiz;

import androidx.lifecycle.ViewModel;

public class QuizViewModel extends ViewModel {



    public final QuestionModel[] questionBank = new QuestionModel[] {
            new QuestionModel(R.string.q1, true),
            new QuestionModel(R.string.q2, false),
            new QuestionModel(R.string.q3, true),
            new QuestionModel(R.string.q4, false),
            new QuestionModel(R.string.q5, true),
            new QuestionModel(R.string.q6, false),
            new QuestionModel(R.string.q7, true),
            new QuestionModel(R.string.q8, false),
            new QuestionModel(R.string.q9, true),
            new QuestionModel(R.string.q10, false),
    };

    public int getQuestion(){
        return questionBank[currentIndex].getQuestion();
    }

    public int punctuation = 0;
    public void increasePuntuation () {
        this.punctuation++;
    }

    public int currentIndex = 0;
    public void increaseIndex (){
        currentIndex++;
    }

    public String getBreadCrumbs(){
        return "Question " + (currentIndex+1)+ " of " +questionBank.length;
    }

    public void restartQuiz(){
        this.punctuation = 0;
        this.currentIndex = 0;
    }
}
